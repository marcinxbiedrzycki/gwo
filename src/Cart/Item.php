<?php

namespace Recruitment\Cart;

use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Entity\Product;

class Item
{
    private $product;
    private $quantity;
    private $totalPrice ;
    private $totalPriceGross;

    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->setQuantity($quantity);
        $this->totalPrice = $this->calculateTotalPrice();
        $this->totalPriceGross = $this->calculateTotalPriceGross();
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        $this->validateQuantity($quantity);

        if ($quantity < $this->product->getMinimumQuantity()) {
            throw new QuantityTooLowException();
        }
        $this->getTotalPrice();
        return $this;
    }

    private function validateQuantity(int $quantity): void
    {
        if ($quantity < $this->product->getMinimumQuantity()) {
            throw new QuantityTooLowException();
        }
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setTotalPrice(int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    public function getTotalPrice(): int
    {
        return $this->calculateTotalPrice();
    }

    public function setTotalPriceGross(int $totalPrice): self
    {
        $this->totalPriceGross = $totalPrice;
        return $this;
    }

    public function getTotalPriceGross(): int
    {
        return $this->calculateTotalPriceGross();
    }

    public function calculateTotalPriceGross(): int
    {
        $totalGross =$this->product->getUnitPriceGross() * $this->getQuantity();
        return $totalGross;
    }

    public function calculateTotalPrice(): int
    {
        $total =$this->product->getUnitPrice() * $this->getQuantity();
        return $total;
    }
}
