<?php

namespace Recruitment\Cart;

use Recruitment\Entity\Order;
use Recruitment\Entity\Product;

class Cart
{
    
    private $items = array();
    private $totalPrice;
    private $totalPriceGross;

    public function addProduct(Product $product, int $quantity = 1): self
    {
        $item = new Item($product, $quantity);

        $this->checkAndSetQuantity($item);
        $this->updateTotalPriceGross();
        $this->updateTotalPrice();
        return $this;
    }

    public function removeProduct(Product $product): void
    {
        $key = array_search($product, $this->items);
        unset($this->items[$key]);
        $this->updateTotalPrice();
        $this->items = array_values($this->items);
    }

    public function setQuantity(Product $product, int $quantity): void
    {
        $item = new Item($product, $quantity);

        $this->updateQuantityOfAnExistingItem($item);
        $this->updateTotalPrice();
    }

    public function getItem(int $index): Item
    {
        if (!array_key_exists($index, $this->items)) {
            throw new \OutOfBoundsException();
        } else {
            return $this->items[$index];
        }
    }

    public function checkout(int $id): Order
    {
        $order = new Order($id, $this->items);

        foreach ($this->items as $item) {
            $remove = $item->getProduct();
            $this->removeProduct($remove);
        }
        if (empty($this->items)) {
            $this->totalPrice = 0;
        }
        return $order;
    }

    public function checkAndAddProduct(Item $item): void
    {
        foreach ($this->items as $existingItem) {
            if ($item->getProduct() === $existingItem->getProduct()) {
                $this->totalPrice = $item->getProduct()->getUnitPrice() *
                    ($item->getQuantity() + $existingItem->getQuantity());
                return;
            }
        }
        $this->items[] = $item;
    }

    public function updateQuantityOfAnExistingItem(Item $item): void
    {
        foreach ($this->items as $existingItem) {
            if ($item->getProduct() == $existingItem->getProduct()) {
                $existingItem->setQuantity($item->getQuantity());
                return;
            } else {
                $this->updateTotalPrice();
                $this->setItems($item);
                return;
            }
        }
        $this->items[] = $item;
    }

    public function checkAndSetQuantity(Item $item): void
    {
        foreach ($this->items as $existingItem) {
            if ($item->getProduct() === $existingItem->getProduct()) {
                $existingItem->setQuantity($item->getQuantity() + $existingItem->getQuantity());
                return;
            }
        }
        $this->items[] = $item;
    }

    public function updateTotalPrice(): int
    {
        $sum = 0;

        foreach ($this->items as $item) {
            $sum += $item->getTotalPrice();
        }
        return $this->totalPrice = $sum;
    }

    public function updateTotalPriceGross(): int
    {
        $sum = 0;

        foreach ($this->items as $item) {
            $sum += $item->getTotalPriceGross();
        }
        return $this->totalPriceGross = $sum;
    }

    public function getTotalPriceGross(): int
    {
        return $this->totalPriceGross;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(Item $item): Item
    {
        return $this->items[] = $item;
    }

    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }
}
