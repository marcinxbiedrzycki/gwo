<?php

namespace Recruitment\Cart\Exception;

class QuantityTooLowException extends \InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Quantity amount set too low');
    }
}
