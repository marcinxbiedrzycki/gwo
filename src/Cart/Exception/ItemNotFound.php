<?php

namespace Recruitment\Cart\Exception;

class ItemNotFound extends \Exception
{
    public function __construct($itemIndex)
    {
        parent::__construct('Item number'.$itemIndex.'not found');
    }
}
