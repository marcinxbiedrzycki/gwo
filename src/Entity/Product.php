<?php

namespace Recruitment\Entity;

use Recruitment\Entity\Exception\InvalidMinimumQuantityException;
use Recruitment\Entity\Exception\InvalidUnitPriceException;

class Product
{

    private const DEFAULT_MINIMUM_QUANTITY = 1;
    private const DEFAULT_UNIT_PRICE = 1;

    private $id;
    private $unitPrice ;
    private $unitPriceGross;
    private $minimumQuantity;
    private $tax = 0;

    public function __construct()
    {
        $this->setMinimumQuantity(self::DEFAULT_MINIMUM_QUANTITY);
        $this->setUnitPrice(self::DEFAULT_UNIT_PRICE);
    }

    public function setUnitPrice(int $unitPrice): self
    {
        $this->validateUnitPrice($unitPrice);
        $this->unitPrice = $unitPrice;
        return $this;
    }

    private function validateUnitPrice(int $unitPrice): void
    {
        if ($unitPrice < self::DEFAULT_UNIT_PRICE) {
            throw new InvalidUnitPriceException();
        }
    }

    public function setMinimumQuantity(int $minimumQuantity): self
    {
        $this->validateMinimumQuantity($minimumQuantity);
        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }

    private function validateMinimumQuantity(int $minimumQuantity): void
    {
        if ($minimumQuantity < self::DEFAULT_MINIMUM_QUANTITY) {
            throw new InvalidMinimumQuantityException();
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    public function setTax(int $tax): self
    {
        $this->tax = $tax;
        return $this;
    }

    public function getTax(): int
    {
        return $this->tax;
    }

    public function calculateUnitPriceGross(): int
    {
        return $this->unitPriceGross = $this->unitPrice + (($this->unitPrice * $this->getTax())/100);
    }

    public function getUnitPriceGross(): int
    {
        return $this->calculateUnitPriceGross();
    }
}
