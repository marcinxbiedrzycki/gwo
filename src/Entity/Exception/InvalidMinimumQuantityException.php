<?php

namespace Recruitment\Entity\Exception;

class InvalidMinimumQuantityException extends \InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Set valid minimum quantity for product.');
    }
}
