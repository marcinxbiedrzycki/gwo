<?php

namespace Recruitment\Entity\Exception;

class InvalidUnitPriceException extends \InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Please provide valid unit price');
    }
}
