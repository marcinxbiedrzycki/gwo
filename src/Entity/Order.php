<?php

namespace Recruitment\Entity;

class Order
{
    private $id;
    private $items = array();

    public function __construct(int $id, array $items)
    {
        $this->id = $id;
        $this->items = $items;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDataForView(): array
    {
        $id = array();
        $total = 0;
        $totalGross = 0;

        foreach ($this->items as $item) {
            $id[] = [
                'id' => $item->getProduct()->getId(),
                'quantity' => $item->getQuantity(),
                'tax' => $item->getProduct()->getTax().'%',
                'total_price' => $item->getTotalPrice(),
                'total_price_gross' => $item->getTotalPriceGross()
            ];
        }

        foreach ($this->items as $item) {
            $total += $item->getTotalPrice();
            $totalGross += $item->getTotalPriceGross();
        }

        $array = [
            'id' => $this->getId(),
            'total_price' => $total ,
            'total_price_gross' => $totalGross,
            'items' => $id
        ];
        return $array;
    }
}
